CC = gnatmake
SOURCE = turing.adb
BIN = turing
CFLAGS = -Wall -Wextra -ggdb -gnat2022 -bargs -Es
BUILD_DIR = ./build/

all: $(SOURCE)
	$(CC) $(SOURCE) -o $(BIN) $(CFLAGS)
