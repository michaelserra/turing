with Ada.Strings;               use Ada.Strings;
with Ada.Strings.Fixed;         use Ada.Strings.Fixed;
with Ada.Strings.Unbounded;     use Ada.Strings.Unbounded;
with Ada.Strings.Maps;          use Ada.Strings.Maps;
with Ada.Text_IO;               use Ada.Text_IO;
with Ada.Exceptions;            use Ada.Exceptions;
with Ada.Command_Line;          use Ada.Command_Line;
with Ada.Containers;            use Ada.Containers;
with Ada.Containers.Vectors;

procedure Turing is

    package Unbounded_String_Vectors is new Ada.Containers.Vectors(Natural, Unbounded_String);
    subtype Tokens_T is Unbounded_String_Vectors.Vector;
    New_Line: constant Character :=  Character'Val(10);

    type Symbol is new Unbounded_String;
    type Direction is (Left, Right);
    type State_T is (Inc, Halt);

    package Symbol_Vectors is new Ada.Containers.Vectors(Natural, Symbol);
    subtype Tape_T is Symbol_Vectors.Vector;

    type Inst_T is record
        State: State_T;
        Read: Symbol;
        Write: Symbol;
        Step: Direction;
        Next: State_T;
    end record;

    package Inst_T_Vectors is new Ada.Containers.Vectors(Natural, Inst_T);
    subtype Program_T is Inst_T_Vectors.Vector;

    Parsing_Error: exception;

    type Machine_T is record
        State: State_T;
        Tape: Tape_T;
        Head: Natural;
        Halt: Boolean;
    end record;

    Runtime_Error: exception;

    function Read_File_Content(File_Path: String) return Unbounded_String is
        F: File_Type;
        Result: Unbounded_String;
    begin
        Open(F, In_File, File_Path);
        while not End_Of_File(F) loop
            Append(Result, Get_Line(F) & New_Line); 
        end loop;
        Close(F);
        return Result;
    end Read_File_Content;

    function Tokenize(Source: String) return Tokens_T is
        Delimeters: constant Character_Set := To_Set(" " & New_Line);
        Index: Natural := 1;
        First: Natural;
        Last: Natural;
        Result: Tokens_T;
        Token: Unbounded_String;
    begin
        while Index in Source'Range loop
            Find_Token(
                Source => Source,
                Set => Delimeters,
                From => Index,
                Test => Outside,
                First => First,
                Last => Last
            );
            exit when Last = 0;

            Token := To_Unbounded_String(Source(First .. Last));
            Result.Append(Token);
            Index := Last + 1;
        end loop;
        return Result;
    end Tokenize;

    function Parse_Symbol(Tokens: Tokens_T; I: in out Natural) return Symbol is
        Result: Symbol;
    begin
        if I >= Natural(Tokens.Length) then
            raise Parsing_Error with ("Unexpected EOF at token " & I'Image);
        end if;
        Result := Symbol(Tokens.Element(I));
        I := I + 1;
        return Result;
    end Parse_Symbol;

    function Parse_Direction(Tokens: Tokens_T; I: in out Natural) return Direction is
        Direction_Str: constant String := To_String(Parse_Symbol(Tokens, I));
    begin
        if Direction_Str = "->" then
            return Right;
        elsif Direction_Str = "<-" then
            return Left;
        else
            raise Parsing_Error with (Direction_Str & " is not a valid direction, use either -> or <-");
        end if;
    end Parse_Direction;

    function Parse_Program(Tokens: Tokens_T) return Program_T is
        I: Natural := 0;
        Result: Program_T;
    begin
        while I < Natural(Tokens.Length) loop
            declare
                State: constant State_T := State_T'Value(To_String(Parse_Symbol(Tokens, I)));
                Read: constant Symbol := Parse_Symbol(Tokens, I);
                Write: constant Symbol := Parse_Symbol(Tokens, I);
                Step: constant Direction := Parse_Direction(Tokens, I);
                Next: constant State_T := State_T'Value(To_String(Parse_Symbol(Tokens, I)));
                New_Inst: constant Inst_T := (State, Read, Write, Step, Next);
            begin
                Result.Append(New_Inst);
            end;
        end loop;
        return Result;
    end Parse_Program;

    function New_Tape return Tape_T is
        Initial_Tape_Len: constant Natural := 8;
        Result: Tape_T;
    begin
        for I in 0 .. Initial_Tape_Len-1 loop
            Result.Append(Symbol'(To_Unbounded_String("0")));
        end loop;
        return Result;
    end New_Tape;

    function New_Machine return Machine_T is
        Result: constant Machine_T := (
            State => Inc, 
            Tape => New_Tape,
            Head => 0,
            Halt => False
        );
    begin
        return Result;
    end;

    procedure Exec_Program(Machine: in out Machine_T; Program: Program_T) is
    begin
        while not Machine.Halt loop
            for Inst of Program loop
                if Machine.Tape(Machine.Head) = Inst.Read and Machine.State = Inst.State then
                    Machine.Tape(Machine.Head) := Inst.Write;
                    if Inst.Step = Left and Machine.Head = 0 then
                        raise Runtime_Error with "Head underflow detected";
                    end if;
                    Machine.Head := (if Inst.Step = Right then Machine.Head + 1 else Machine.Head - 1);
                    Machine.State := Inst.Next;
                    if Machine.State = Halt then
                        Machine.Halt := True;
                    end if;
                    Put_Line("Instruction triggered: " & Inst.State'Image);
                    Put_Line(Machine'Image);
                end if;
            end loop;
        end loop;
    end Exec_Program;

    -----------------------------------------------------------------------------------------

    procedure Print_Usage is
    begin
        Put_Line("Usage: " & Command_Name & " <file.tur>");
    end Print_Usage;

    procedure Die(Msg: String := "") is
    begin
        Put_Line(Standard_Error, Msg);
        Set_Exit_Status(Failure);
    end Die;

    File_Path: Unbounded_String;
    Source: Unbounded_String;
    Tokens: Tokens_T;
    Program: Program_T;
    Machine: Machine_T;

begin
    if Argument_Count < 1 then
        Print_Usage;
        Die("ERROR: At least source file expected");
        return;
    end if;

    File_Path := To_Unbounded_String(Argument(1));
    begin
        Source := Read_File_Content(To_String(File_Path));
    exception
        when E: others =>
            Die("ERROR: " & Exception_Message(E));
            return;
    end;

    Tokens := Tokenize(To_String(Source));
    begin
        Program := Parse_Program(Tokens);
    exception
        when E: others =>
            Die("ERROR: Unable to parse " & To_String(File_Path) & ": " & Exception_Message(E));
            return;
    end;

    Machine := New_Machine;
    declare
        New_Value: constant Symbol := To_Unbounded_String("1");
    begin
        Machine.Tape(Natural(Machine.Tape.Length - 1)) := New_Value;
        Exec_Program(Machine, Program);
    exception
        when E: Runtime_Error =>
            Die("RUNTIME ERROR: " & Exception_Message(E));
            return;
    end;

end Turing;
